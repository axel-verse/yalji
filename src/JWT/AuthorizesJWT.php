<?php

namespace AxelVerse\YALJI\JWT;

use Illuminate\Http\Request;
use App\Models\User;
use Firebase\JWT\JWT;
use Illuminate\Support\Str;

trait AuthorizesJWT
{
    private function jwtResponse(Request $request, User $user)
    {
        $t = now();
        $id = $user->uuid;
        $payload = $payloadRefresh = [
            'iat' => $t->unix(),
            'nbf' => $t->unix(),
            'jti' => Str::uuid(),
            'exp' => $t->addMinutes(5)->unix(),
            'identity' => [
                'id' => $id,
                'ip' => $request->ip()
            ]
        ];

        $payload['type'] = 'access';

        $payloadRefresh['exp'] = $t->addMonths(1)->unix();
        $payloadRefresh['type'] = 'refresh';

        $access = JWT::encode($payload, config('keys.encryption_key'));
        $refresh = JWT::encode($payloadRefresh, config('keys.encryption_key'));

        return response()->json([
            'access' => $access,
            'refresh' => $refresh
        ], 200);
    }
}
