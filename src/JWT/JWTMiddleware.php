<?php

namespace AxelVerse\YALJI\JWT;

use Closure;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Firebase\JWT\JWT as JwtCoder;

class JWTMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param  Closure  $next
     * @return JsonResponse|mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->bearerToken()) {
            try {
                $jwt = JwtCoder::decode($request->bearerToken(), config('keys.encryption_key'), ['HS256']);
            } catch (Exception $exception) {
                $jwt = NULL;
            }
            if (
                $jwt
                && $jwt->exp
                && $jwt->exp - now()->unix() > 0
                && $jwt->identity
                && $jwt->identity->id
            ) {
                $request->identity = $jwt->identity;
                return $next($request);
            }
        }

        return response()->json([
            'message' => '401 Not Authorized'
        ], 401);
    }
}
