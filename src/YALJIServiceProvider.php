<?php

namespace AxelVerse\YALJI;

use Illuminate\Support\ServiceProvider;

class YALJIServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/yalji.php', 'yalji');
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../config/yalji.php' => config_path('yalji.php')
        ], 'config');
    }
}
