<?php

return [

    /**
     * JWT encryption key. If not provided, APP_KEY will be used
     */
    "key" => env('YALJI_KEY', env('APP_KEY')),

    /**
     * Choose either stateful or stateless mode
     */
    "state_mode" => env('YALJI_STATE_MODE', 'stateless'),

    /**
     * Choose either single or multi endpoints
     */
    "endpoints_mode" => env('YALJI_ENDPOINTS_MODE', 'single'),

    /**
     * Choose model to auth with. Full class name required
     */

    "model" => env('YALJI_AUTH_MODEL', 'App\Models\User'),

    /**
     * Choose model's unique property to auth with
     */
    "prop" => env('YALJI_AUTH_PROP', 'id')

];
